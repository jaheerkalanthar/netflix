import React,{useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux"
import HomeScreen from "./Screens/HomeScreen"
import LoginScreen from './Screens/LoginScreen'
import {login,logout, selectUser} from './features/userSlice'
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';//React Router is used for connect the Pages with certain Page
import { auth } from './firebase';
import Profile from './Screens/Profile';


function App() {
  const dispatch=useDispatch();
  const user=useSelector(selectUser)


  //The Below useEffect is Listen the User logged in that page or not
  useEffect(()=>{
   const unsubscribe=auth.onAuthStateChanged((userAuth)=>{
     ///unsubscribe is a useEffect function for Unmount
     if (userAuth){
       //Logged In
       dispatch(login({
         uid:userAuth.uid,
         email:userAuth.email,
       }))

     }
     else{
       //Logged Out
       dispatch(logout())

     }
   });
   return unsubscribe
  },[])
  return (
    <div className="app">
      
      <Router>
        {!user ? (<LoginScreen></LoginScreen>) :(
        <Switch>
          <Route path='/profile'>
            <Profile></Profile>
          </Route>
          <Route exact path="/">
            <HomeScreen></HomeScreen>
          </Route>
        </Switch>)}
        
      </Router>
      
           
    </div>
  );
}

export default App;
