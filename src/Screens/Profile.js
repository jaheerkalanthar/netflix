import "./Profile.css"
import React from 'react'
import NavBar from "./subComponent/NavBar"
import { useSelector } from "react-redux"
import { selectUser } from "../features/userSlice"
import { auth } from "../firebase"
import PlansScreen from "./PlansScreens"

function Profile() {
    const user=useSelector(selectUser)
    return (
        <div className="profile">
            
            <NavBar></NavBar>
            <div className="profileBody">
                <h1>Edit Profile</h1>
                <div className="profileInfo">
                    <img className="avatar2" src="https://i.pinimg.com/originals/0d/dc/ca/0ddccae723d85a703b798a5e682c23c1.png"
                    alt=""></img>
                    <div className="gradiant"></div>
                    <div className="profileDetails">
                        <h2>{user.email}</h2>
                        <div className="profilePlans">
                            <h3>Plans</h3>
                            <div className="plansScreen">
                                <PlansScreen></PlansScreen>
                            </div>
                            <button onClick={()=>auth.signOut()} className="profileSignOut">Sign Out</button>
                        </div>
                        
                    </div>
                </div>

                
            </div>
        </div>
    )
}

export default Profile
