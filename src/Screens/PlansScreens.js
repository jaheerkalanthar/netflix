import React, { useState,useEffect } from 'react'
import { useSelector } from 'react-redux'
import { selectUser } from '../features/userSlice'
import db from '../firebase'
import "./PlansScreens.css"
import {loadStripe} from '@stripe/stripe-js'

function PlansScreens() {
    const [products, setProducts] = useState([])
    const user=useSelector(selectUser)
    const [subsription, setSubsription] = useState(null)

    useEffect(() => {
        db.collection("products").
        where("active","==",true).get().then((querySnapshot)=>{
        const products={};
        querySnapshot.forEach(async (productDoc)=>{
            products[productDoc.id]=productDoc.data();
            const priceSnap=await productDoc.ref.collection('prices').get();
            priceSnap.docs.forEach((price)=>{
                products[productDoc.id].prices={
                    priceId:price.id,
                    priceData:price.data()
                }
            })
        });
        setProducts(products);
        });
    }, [])
    useEffect(()=>{
        db.collection('customers').doc(user.uid).collection('subscriptions').get().then((querySnapshot)=>{
            querySnapshot.forEach(async (subsription)=>{
                setSubsription({
                    role:subsription.data().role,
                    current_period_end:subsription.data().current_period_end.seconds,
                    current_period_start:subsription.data().current_period_start.seconds,
                });
            });
        });
    
        },[user.uid]);
    const loadCheckOut =async (Data)=>{
        const docRef=await db.collection('customers').doc(user.uid).collection('checkout_sessions').add(
            {
                price:Data,
                success_url:window.location.origin,//If the Payment is Success,
                cancel_url:window.location.origin//If the Payment is Succes
            }
        )
        docRef.onSnapshot(async(snap)=>{
            const {error,sessionId}=snap.data();
            if (error){
                //Show An errror to the person
                //inspect your cloud Function log in firebase Console
                alert(`An error is Occured${error.message}`)
            }
            if (sessionId){
                //lets redirect to checkout
                //init Stripe
                const stripe=await loadStripe('pk_test_51ItwG6SJNy7GrELfKM4QnHe3RbhAoh3fBzDebBWlViqr8tawslHvCaB0Jw0KCjtA6kqZNncSOz6YtOplfFs76lRf00Rp4CvlbF'
                );
                stripe.redirectToCheckout({sessionId})
            }
        })
    };
    
    console.log(subsription)
    return (

        <div className="plansScreens">
            {subsription&&(<p>Renewal Date:{" "}
            {new Date(subsription?.current_period_end*1000).toLocaleDateString()}</p> )}
            {Object.entries(products).map(([productId,productData])=>{
                //check user subscription is Active Or Not
                const  isCurrentPackage=productData.name?.toLowerCase()?.includes(subsription?.role)

                return(
                    <div key={productId} className={`${isCurrentPackage && "planDisabled"} planScreenPlans`}>
                        <div className="planScreenInfo">
                            <h5>{productData.name}</h5>
                            <h6>{productData.description}</h6>
                            
                        </div>
                        <button onClick={()=>!isCurrentPackage&& loadCheckOut(productData?.prices?.priceId)}>
                            {isCurrentPackage?"Current Package":" Subscribe"}</button>
                    </div>

                )
            })}
            
        </div>
    )
}

export default PlansScreens
