import React,{useRef,useState} from 'react'
import "./SignIn.css"
import {auth } from "/home/raven-tech/Documents/ReactApp/netflix/src/firebase"




function SignIn() {
    const emailRef=useRef(null)
    const passWordRef=useRef(null)
    
    const register=(e)=>{
        e.preventDefault();
        auth.createUserWithEmailAndPassword(
            emailRef.current.value,
            passWordRef.current.value
        ).then((authUser)=>{
            console.log(authUser)
        }).catch((error)=>{alert(error.message)})

    }
    const signIn=(e)=>{
        e.preventDefault();
        auth.signInWithEmailAndPassword(
            emailRef.current.value,
            passWordRef.current.value
        ).then((authUser)=>{
            console.log(authUser)
        }).catch((error)=>{alert(error.message)})
        
    }
    
   
   
    return (
        <div className="signInScreen">
            
                
                <form className="form">
                    <h1>Sign In</h1>
                    <input name="email" ref={emailRef} type="email" placeholder="Email address"></input>
                    <input name="password" ref={passWordRef}   type="password"  placeholder="Password"></input>
                    <button className="signInbutton" onClick={signIn}>Sign In</button>
                    <h4 className="SignInsignUp"><span className="grey">New to the Netflix? </span> <span onClick={register} className="curserPoint"> Sign Up Now</span></h4>
                </form>
            
        </div>
    )
}

export default SignIn
