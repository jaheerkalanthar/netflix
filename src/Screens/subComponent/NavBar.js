import React,{useEffect,useState} from 'react'
import { useHistory } from 'react-router'
import Profile from '../Profile'
import './NavBar.css'


function NavBar() {
    const [show, setShow] = useState(false)
    const history=useHistory()
    const transitionNavBar=()=>{
        if (window.scrollY>100){
            setShow(!show)//If i scrool more than 100px the Black Background will pop ups
        }
        else{
            setShow(show)//It is Default transparent color
        }
    }
    useEffect(() => {
        window.addEventListener("scroll",transitionNavBar);//It will wait untill i scroll something
        return ()=>{
            window.removeEventListener("scroll",transitionNavBar)
        }
    }, [])//[]->Empty because it only runs one components At once
    return (
        <div className={`navBar ${show && "navBarBlack"}`}>
            <div className="navbarDesign">
                
                    <img className="logoImage" onClick={()=>history.push("/")}
                    src="https://www.freepnglogos.com/uploads/netflix-logo-0.png" ></img>            
                
                
                    <img  className="avatar" onClick={()=>history.push("/profile")}
                    src="https://i.pinimg.com/originals/0d/dc/ca/0ddccae723d85a703b798a5e682c23c1.png"
                    ></img>
                    
                
            </div>
        </div>
    )
}

export default NavBar;
