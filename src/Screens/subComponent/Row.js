import React,{useState,useEffect} from 'react'
import axios from './Banner/axios'
import './Row.css'
function Row({name ,fetchUrl,isLargeRow}) {
    const [movies,setMovies]=useState([]);
    const baseUrl="https://image.tmdb.org/t/p/original/"

    
    useEffect(()=>{
        async function fetchData() {
            const request=await axios.get(fetchUrl);
            setMovies(request.data.results)
            return request
        }
        fetchData();
    },[fetchUrl]);
    console.log(movies)
    return (
        <div className="row">
            <h1>{name}</h1>
            <div className="insideRow">
                <React.Fragment className="fragmentElement">
                    {movies.map(movie=>(
                        <div className="insideElement">
                            <img className={`rowImage ${isLargeRow && "rowImageLarge"}`}  src={`${baseUrl}${
                                isLargeRow ? movie.poster_path : movie.backdrop_path
                            }`} alt={movie.name}></img>  
                            <h3 className="rowName">{movie.title|| movie.name}</h3>
                        
                            
                        </div>
                    ))}
                </React.Fragment>  

            </div>                
        </div>
    )
}

export default Row
