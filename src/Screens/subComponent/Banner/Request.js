const API_KEY="d527591cc8ecd5bdc21a3f97256d5adb"

const requests={
    fetchTrending:`/trending/all/week?api_key=${API_KEY}&language=en-US`,
    fetchNetflixOriginals:`/discover/tv/?api_key=${API_KEY}&with_networks=213`,
    fetchTopRated:`/movie/top_rated?api_key=${API_KEY}&language=en-US`,
    fetchActionMovies:`/discover/movie/?api_key=${API_KEY}&with_genres=28`,
    fetchComedyMovies:`/discover/movie/?api_key=${API_KEY}&with_genres=35`,
    fetchHorrorMovies:`/discover/movie/?api_key=${API_KEY}&with_genres=27`,
    fetchRomanceMovies:`/discover/movie/?api_key=${API_KEY}&with_genres=10749`,
    fetchDocumentries:`/discover/movie/?api_key=${API_KEY}&with_genres=99`,
};


export default requests;
/*
For Exaample

website link:"https://api.themoviedb.org/3"

Secondary Url:"/trending/all/week?api_key=${API_KEY}&language=en-US"

Now while searching something the Website will Appear Like below

https://api.themoviedb.org/3/trending/all/week?api_key=${API_KEY}&language=en-US

this will return some Information

*/