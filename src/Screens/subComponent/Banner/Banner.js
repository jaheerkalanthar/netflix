import React,{useState,useEffect } from 'react'
import './Banner.css'
import axios from './axios'
import requests from './Request'
function Banner() {

    const [movie,setMovie]=useState([])

    useEffect(()=>{
        async function fetchData(){
            const request=await axios.get(requests.fetchActionMovies);
            setMovie(
                request.data.results[
                    Math.floor(Math.random()*request.data.results.length-1)
                ]
            )
        }
        fetchData();
    },[])

    const trauncate=(string,numberOfWords)=>{
        //This will reduce the number of words by ....
        return string?.length>numberOfWords ? string.substr(0,numberOfWords-1)+"....":string //? is used for exception
    }
    return (
        <header className="banner" style={{
            backgroundSize:"cover",
            backgroundImage:`url("https://image.tmdb.org/t/p/original/${movie?.backdrop_path}")`,
            backgroundPosition:"center center"
        }}>
            <div className="bannerContent">
                <h1 className="bannerTitle">{movie?.title || movie?.name || movie?.original}</h1>
                <div className="bannerButton">
                    <button className="button">Play</button>
                    <button className="button">My List</button>
                </div>
                <h3 className="bannerDescription">{trauncate(movie.overview,150)}</h3>
            </div>   
            <div className="faddedBottom"></div>  
        </header>
          
            
    )
}

export default Banner
