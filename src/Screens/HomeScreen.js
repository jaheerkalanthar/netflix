import React from 'react'
import NavBar from './subComponent/NavBar'
import Banner from './subComponent/Banner/Banner'
import Row from './subComponent/Row'
import requests from './subComponent/Banner/Request'
//Now ,we are going to create 
//1.Nav Bar
//2.Banner 
//3.Rows
function HomeScreen() {

    return (
        <div className="homeScreen">
            
            <NavBar></NavBar>
            <Banner></Banner>
            <Row name="Netflix Originals" isLargeRow fetchUrl={requests.fetchNetflixOriginals}></Row>
            <Row name="TopRated"  fetchUrl={requests.fetchTopRated}></Row>
            <Row name="Trending"  fetchUrl={requests.fetchTrending}></Row>
            <Row name="Action Movies"  fetchUrl={requests.fetchActionMovies}></Row>
            <Row name="Comedy Movies"  fetchUrl={requests.fetchComedyMovies}></Row><Row name="Action Movies"  fetchUrl={requests.fetchActionMovies}></Row>
            <Row name="Horror Movies"  fetchUrl={requests.fetchHorrorMovies}></Row>
            <Row name="Romance Movies"  fetchUrl={requests.fetchRomanceMovies}></Row>
            <Row name="Documentries"  fetchUrl={requests.fetchDocumentries}></Row>

        </div>
    )
}

export default HomeScreen;
