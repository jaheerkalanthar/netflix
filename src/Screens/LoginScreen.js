import React,{useState} from 'react'
import './LoginScreen.css'
import SignIn from './SignProperties/SignIn'
import SignUp from './SignProperties/SignUp'

function LoginScreen() {
    const [signIn,setSignIn]=useState(false)
    const [signUp,setSignUp]=useState(false)
    return (
        <div className="loginScreen">
            <div className="loginPage">
                <img className="logo"
                    src="https://www.freepnglogos.com/uploads/netflix-logo-0.png" ></img>  
                {!signIn && <button onClick={()=>setSignIn(!signIn)} className="signIn">Sign In</button>}
                <div className="loginGradient"></div>
                
            </div>
            <div className="loginDiv">
                {!signIn && !signUp ? 
                (<>
                <h1>Unlimited films,TV Programmes and more.</h1>
                 <h2>Watch anywhere, Cancel at anytime</h2> 
                 
                <form>
                    <h3>Ready to watch?Enter your email to create to restart your membership</h3>
                    <input type="email" placeholder="Email Address"></input>
                    <button className="loginButton" onClick={()=>setSignUp(!signUp)}>Get Started</button>
                 </form> 
                </>):""}
                {signIn ? (<SignIn></SignIn>):""}
                {signUp ? (<SignUp></SignUp>):""}

                
            </div>
            
            

        </div>
    )
}

export default LoginScreen

